FROM java:8

ADD target/demo-server-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

ENTRYPOINT [ "sh", "-c", "java -jar /app.jar" ]