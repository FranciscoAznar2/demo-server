package com.okta.developer.test.steps;


import com.okta.developer.test.dto.User;
import com.okta.developer.test.utils.BrowserDriver;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginSteps {


    private BrowserDriver browserDriver;
    private User user;
    private WebDriver driver;

    public LoginSteps() {
        browserDriver = new BrowserDriver();
    }


    @Given("^the user is not registered$")
    public void userWithoutPermissions() throws InterruptedException {
        user = new User("Pepe", "admin");
        driver = browserDriver.getCurrentDriver();
        driver.manage().window().maximize();
        driver.get("http://localhost:4200");
        Thread.sleep(3000);
        WebElement loginBtn = driver.findElement(By.cssSelector("body > app-root > app-home > mat-card > mat-card-content > button > span"));
        loginBtn.click();
        Thread.sleep(3000);
        WebElement redirectBtn = driver.findElement(By.cssSelector("#content > div.error-wrap.clearfix > div.error-content > div > a"));
        redirectBtn.click();
        Thread.sleep(3000);
    }

    @When("^he tries to do login$")
    public void goToHomePage() throws InterruptedException {
        WebElement usernameField = driver.findElement(By.cssSelector("#okta-signin-username"));
        WebElement passwordField = driver.findElement(By.cssSelector("#okta-signin-password"));
        WebElement submitBtn = driver.findElement(By.cssSelector("#okta-signin-submit"));
        usernameField.sendKeys(user.getName());
        passwordField.sendKeys(user.getPassword());
        submitBtn.click();
        Thread.sleep(3000);
    }

    @Then("^the website response with a alert message$")
    public void assertSecurityRedirect(String message) {
        WebElement alertMessage = driver.findElement(By.cssSelector("div.o-form-error-container.o-form-has-errors > div > div > p"));
        Assert.assertEquals(alertMessage.getText(), message);
        driver.close();
    }

    @Then("^the website response with a success message$")
    public void assertSuccess(String message) {
        WebElement alertMessage = driver.findElement(By.cssSelector("div.o-form-error-container.o-form-has-errors > div > div > p"));
        Assert.assertNotEquals(alertMessage.getText(), message);
        driver.close();
    }

}
