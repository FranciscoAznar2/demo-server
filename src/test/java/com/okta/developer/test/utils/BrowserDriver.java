package com.okta.developer.test.utils;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowserDriver {

    private WebDriver mDriver;

    public WebDriver getCurrentDriver() {
        if (mDriver == null) {
            initDriver();
        } else if (mDriver instanceof RemoteWebDriver && ((RemoteWebDriver) mDriver).getSessionId() == null) {
            initDriver();
        } else if (mDriver instanceof ChromeDriver && ((ChromeDriver) mDriver).getSessionId() == null) {
            initDriver();
        }
        return mDriver;
    }

    public void initDriver() {

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--no-sandbox");
        try {
            mDriver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), chromeOptions);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            ChromeDriverManager.chromedriver().setup();
            mDriver = new ChromeDriver(chromeOptions);
        }
    }

}
